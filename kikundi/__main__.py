#############################################
#############################################
###    _  ___ _                    _ _    ###
###   | |/ (_) | ___   _ _ __   __| (_)   ###
###   | ' /| | |/ / | | | '_ \ / _` | |   ###
###   | . \| |   <| |_| | | | | (_| | |   ###
###   |_|\_\_|_|\_\\__,_|_| |_|\__,_|_|   ###
###                                       ### 
#############################################
###=======================================###
###   v1.0 | 2019/01/29 | Thomas Nuttall  ###
###=======================================###
#############################################
 
import sys

import click

import kikundi.evaluation
import kikundi.feature_extraction
import kikundi.model
import kikundi.persistence
import kikundi.utils

logger = kikundi.utils.get_logger(__name__)


@click.group()
def cli():
    pass


@cli.command(name="run-pipeline")
@click.argument('config-filenames', nargs=-1)
@click.option('--report-on-test/--do-not-report-on-test', default=False, help='Output evaluation metrics on test dataset?')
def cmd_run_pipeline(config_filenames, report_on_test=False):
    run_pipeline(config_filenames=config_filenames, report_on_test=report_on_test)


def run_pipeline(config_filenames, report_on_test):
    """
    Run full task pipeline
    """
    kikundi.utils.package_banner() 

    logger.info('Commencing run...')
    conf_contents = kikundi.utils.load_configuration(config_filenames)

    ###################
    #### DATA LOAD ####
    ###################

    # load 4 datasets

    # compute intersection on recording ID


    ###################################
    #### FEATURE/TARGET EXTRACTION ####
    ###################################

    # Filter subgenres if there are too many

    # compute track-subgenre occurence matrix

    # compute distance matrix
        # cosine distance
        # manhattan
        # etc...


    ##########################
    #### TRAIN/TEST SPLIT ####
    ##########################

    # is this necessary here?

    
    ###################
    #### MODELLING ####
    ###################

    # Hiearchichal clustering to cluster subgenres


    ###################
    #### EVALUATION ###
    ###################

    # entropy

    # within cluster variance

    # between cluster variance

    # Distance measures


    ##################
    #### REPORTING ###
    ##################

    # dendrograms

    # metrics graphs

    # low dimensional visualisation

    # size etc...


if __name__ == '__main__':
    cli()
