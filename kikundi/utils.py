import datetime
import hashlib
import logging

import pandas as pd


def get_logger(name):
    logging.basicConfig(format="[%(asctime)s] %(levelname)s [%(name)s.%(funcName)s:%(lineno)d] %(message)s",)
    logger = logging.getLogger(name)
    logger.setLevel(logging.DEBUG)
    return logger


logger = get_logger(__name__)


class ConfigError(Exception):
    pass


def package_banner():
	banner = """


          _  ___ _                    _ _ 
         | |/ (_) | ___   _ _ __   __| (_)
         | ' /| | |/ / | | | '_ \ / _` | |
         | . \| |   <| |_| | | | | (_| | |
         |_|\_\_|_|\_\\__,_|_| |_|\__,_|_|
                                          
        

	""".split("\n")
	for l in banner:
		logger.info(l)


def sha1(s):
	"""Hash input string, <s> to SHA-1"""
	return hashlib.sha1(str(s)).hexdigest()


def load_configuration(config_filenames):
    """
    Load and merge config dicts from list of strings
    """
    import kikundi.persistence
    if not config_filenames:
        raise ConfigError("No config files!")

    # Load configuration
    conf_contents = {}
    for fname in config_filenames:
        logger.info('Loading config file {}'.format(fname))
        f = kikundi.persistence.load_yaml(fname)
        # TODO: ensure duplicate keys are handled correctly
        conf_contents.update(f)

    for c in ['data', 'features', 'model', 'evaluation']:
        assert c in conf_contents,'{}.yaml required to run pipeline'.format(c)

    return conf_contents
