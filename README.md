## Package 

### Documentation

See `documentation/`


### Installation

package is written in Python 3.5

To install package:
`pip install -e .`

### Usage Instructions


### Testing

To run tests:
`pytest`


Thomas Nuttall -  2019